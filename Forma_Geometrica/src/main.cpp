#include <iostream>
#include "Circulo.cpp"
#include "Triangulo.cpp"
#include "Quadrado.cpp"
#include "losango.cpp"
#include "FormaGeometrica.cpp"

int int main(int argc, char const *argv[])
{
  FormaGeometrica forma1;
  Triangulo Triangulo1;
  Quadrado Quadrado1;

  Quadrado Quadrado2(4.0, 4.0);
  Triangulo Triangulo1(2.5, 2.0);
  Circulo Circulo(3.0, 3.0);
  Losango Losango(6.0, 8.0);

  forma1.calcula_area();
  forma1.calcula_perimetro();

  return 0;
}
