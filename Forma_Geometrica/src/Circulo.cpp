#include "FormaGeometrica"
#include <iostream>
#include <cmath.h>

Circulo::Circulo()
{
  set_tipo("Circulo");
  set_base(5.5);
  set_altura(get_base());
}
Circulo::Circulo(float base, float altura)
{
  if(base != altura)
    throw(1);
  set_base(base);
  set_altura(altura);
}
Circulo::~Circulo()
{
  cout << "Destruindo o Circulo: " << tipo << endl;
}
void Circulo::set_base( float base)
{
  this->base = base;
}
void Circulo::get_base()
{
  return base;
}
void Circulo::set_altura(float altura)
{
  this->altura = altura;
}
void Circulo::get_altura()
{
  return altura;
}
float Circulo::calcula_area()
{
  return M_PI*pow(float base, 2);
}
float calcula_perimetro()
{
  return 2*M_PI*base;
}
