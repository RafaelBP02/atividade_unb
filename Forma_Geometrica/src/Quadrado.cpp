#include "Quadrado.hpp"

Quadrado::Quadrado()
{
  set_tipo("Quadrado");
  set_base(5.5);
  set_altura(get_base());
}
Quadrado::Quadrado(float base, float altura)
{
  if(base != altura)
    throw(1);
  set_base(base);
  set_altura(altura);
}
Quadrado::~Quadrado()
{
  cout << "Destruindo o objeto: " << tipo << endl;
}
