#include "FormaGeometrica"
#include "losango.hpp"
#include <iostream>
#include <math.h>

Losango::Losango()
{
  set_tipo("Losango");
  set_base(4.5);
  set_altura(8.5);
}
Losango::Losango(float base, float altura)
{
  set_tipo = "Losango";
  this->base = base;
  this->altura = altura;
}
Losango::~Losango()
{
  cout << "Destruindo o Losango: " << tipo << endl;
}
void Losango::set_base( float base)
{
  this->base = base;
}
void Losango::get_base()
{
  return base;
}
void Losango::set_altura(float altura)
{
  this->altura = altura;
}
void Losango::get_altura()
{
  return altura;
}
float Losango::calcula_area()
{
  return (base * altura)/2;
}
float calcula_perimetro()
{
  return ((pow ( float base, 2))+(pow (float altura, 2))) * 4;
}
