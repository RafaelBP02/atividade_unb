#include "FormaGeometrica"
#include <iostream>
#include <math.h>

Triangulo::Triangulo()
{
  set_tipo("Triangulo");
  set_base(5.0);
  set_altura(8.0);
}
Triangulo::Triangulo(float base, float altura)
{
  set_tipo = "Triangulo";
  this->base = base;
  this->altura = altura;
}
Triangulo::~Triangulo()
{
  cout << "Destruindo o objeto: " << tipo << endl;
}
void Triangulo::set_base( float base)
{
  this->base = base;
}
void Triangulo::get_base()
{
  return base;
}
void Triangulo::set_altura(float altura)
{
  this->altura = altura;
}
void Triangulo::get_altura()
{
  return altura;
}
