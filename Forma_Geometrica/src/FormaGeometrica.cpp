#include "FormaGeometrica.hpp"
#include <iostream>

FormaGeometrica::FormaGeometrica()
{

  base = 10.0;
  altura = 5.0;

}
FormaGeometrica::FormaGeometrica(float base, float altura);
{
  tipo = "Genérico";
  this->base = base;
  this->altura = altura;
}
FormaGeometrica::FormaGeometrica(string tipo, float altura)
{
  this->tipo = tipo;
  this->altura = altura;

}
FormaGeometrica::~FormaGeometrica()
{
  cout << "Destruindo o objeto"
}
void FormaGeometrica::set_tipo(string tipo)
{
  this->tipo = tipo;
}
string FormaGeometrica::get_tipo()
{
  return tipo;
}

void FormaGeometrica::set_base(float base)
{
  this->base = base;
}
float FormaGeometrica::get_base()
{
 return base;
}

void FormaGeometrica::set_altura(float altura)
{
  this->altura = altura;
}
float FormaGeometrica::get_altura()
{
  return altura;
}

float FormaGeometrica::calcula_area()
{
  return base * altura;
}
float FormaGeometrica::calcula_perimetro()
{
  return 2*base + 2*altura;
}
