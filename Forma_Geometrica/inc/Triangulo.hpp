#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "FormaGeometrica"

class Triangulo : public FormaGeometrica
{
public:
  Triangulo();
  Triangulo(float base, float altura);
  ~Triangulo();
  float calcula_area;
  float calcula_perimetro;
}

#endif
