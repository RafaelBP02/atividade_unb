#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "FormaGeometrica"

class Circulo : public FormaGeometrica
{
public:
  Circulo();
  Circulo(float base, float altura);
  ~Circulo();
  float calcula_area();
  float calcula_perimetro();
};

#endif
