#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "FormaGeometrica"

class Quadrado : public FormaGeometrica
{
  public:
    Quadrado();
    Quadrado(float base, float altura);
    ~Quadrado();
    float calcula_area();
    float calcula_perimetro();
};

#endif
