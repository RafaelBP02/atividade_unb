#ifndef LOSANGO_HPP
#define LOSANGO_HPP

#include "FormaGeometrica"

class Losango : public FormaGeometrica
{
public:
  Losango();
  Losango(float base, float altura);
  ~Losango();
  float calcula_area();
  float calcula_perimetro();
};

#endif
